#!/bin/bash

docker volume inspect schemaverse_volume > /dev/null 2>&1

if [ $? -eq 1 ]; then
	echo "Creating \"schemaverse_volume\" volume..."
	docker volume create schemaverse_volume
fi

docker run -d \
	-p 5432:5432 \
	-v schemaverse_volume:/var/lib/postgresql \
	--name=schemaverse_server \
	frozenfoxx/schemaverse:latest
