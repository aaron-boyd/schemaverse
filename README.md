AWS EC2 Startup Script

Using: Ubuntu Server 20.04 LTS (HVM), SSD Volume Type
 
```
#!/bin/bash

apt update
apt upgrade -y
apt install -y docker.io git
usermod -a -G docker ubuntu
systemctl enable docker
systemctl start docker
git clone https://gitlab.com/aaron-boyd/schemaverse.git /home/ubuntu/schemaverse
docker pull frozenfoxx/schemaverse:latest
```